import cv2 as cv
import face_recognition
import numpy as np
import tensorflow as tf
from tensorflow.python.keras.models import model_from_json

frame_resizing = 0.25
class_dict = {'angry': 0, 'disgust': 1, 'fear': 2, 'happy': 3, 'neutral': 4, 'sad': 5, 'surprise': 6}
# class_dict = {'anger': 0, 'contempt': 1, 'disgust': 2, 'fear': 3, 'happy': 4, 'neutral': 5, 'sad': 6, 'surprise': 7}
file_model = 'model/fer'
img_height = 48
img_width = 48

def detect_face_location(frame_face):
    small_frame = cv.resize(frame_face, (0, 0), fx=frame_resizing, fy=frame_resizing)
    rgb_small_frame = cv.cvtColor(small_frame, cv.COLOR_BGR2RGB)
    face_locations = face_recognition.face_locations(rgb_small_frame)
    face_locations = np.array(face_locations)
    face_locations = face_locations / frame_resizing
    return face_locations.astype(int)


li = list(class_dict.keys())

json_file = open(file_model + '.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
model = model_from_json(loaded_model_json,
                        custom_objects={'BatchNormalization': tf.keras.layers.BatchNormalization})
model.load_weights(file_model + ".h5")

cap = cv.VideoCapture(0)

# Prediction image
# img = cv.imread("inputs/train/angry/Training_3908.jpg", cv.IMREAD_GRAYSCALE)
# img = img.reshape(1, img.shape[0], img.shape[1], 1)
# prediction = model.predict(img)
# y_classes = int(prediction.argmax(axis=-1).flatten())
# class_name = li[y_classes]
# print('Class name =', class_name)

while True:
    ret, frame = cap.read()
    resized = cv.resize(frame, (img_width, img_height))
    gray_img = cv.cvtColor(resized, cv.COLOR_RGB2GRAY)
    img = gray_img.reshape(1, img_width, img_height, 1)
    prediction = model.predict(img)
    y_classes = int(prediction.argmax(axis=-1).flatten())
    class_name = li[y_classes]

    locations = detect_face_location(frame)
    for location in locations:
        top, right, bottom, left = location[0], location[1], location[2], location[3]
        cv.putText(frame, class_name, (left + 135, top - 20), cv.FONT_HERSHEY_DUPLEX, 1, (0, 0, 200), 2)
        cv.rectangle(frame, (left, top), (right, bottom), (0, 0, 200), 4)

    cv.imshow("Facial Emotion Recognition", frame)

    key = cv.waitKey(1)
    if key == 27:
        break
cap.release()
cv.destroyAllWindows()
